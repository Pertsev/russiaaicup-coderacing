from model.Car import Car
from model.Game import Game
from model.Move import Move
from model.World import World
from model.Bonus import Bonus
from math import pi, hypot, sqrt, atan2, copysign
from util import PriorityQueue, FixedQueue
from turn import Turn

class MyStrategy:
    def __init__(self):
        self.current_tilepoint = ['x', 'y']
        self.second_tilepoint = ['x', 'y']
        self.third_tilepoint = ['x', 'y']
        self.fourth_tilepoint = ['x', 'y']
        self.fifth_tilepoint = ['x', 'y']
        self.last_tilepoint = ['x', 'y']
        self.last_waypoint = ['x', 'y']
        self.current_turn = ''
        self.last_turn = ''
        self.path = []
        self.invisible = False
        self.world = ''
        self.me = ''
        self.game = ''
        self._move = ''
        self.use_oil_in_next_turn = False
        self.oil_canister_count = 0
        self.next_waypoint_number = 0
        self.speed_track = FixedQueue(10)
        self.stop = False
        self.u_turn_counter = 500
        self.near_bonus = None
        self.bonus_timer = 0
        self.bonus_list = set()
        top = [1, 3, 4, 7, 8, 10, 11]
        right = [2, 4, 6, 7, 9, 10, 11]
        bottom = [1, 5, 6, 7, 8, 9, 11]
        left = [2, 3, 5, 8, 9, 10, 11]

        self.accessible_tiles = {
            0: {'top': [], 'right': [], 'bottom': [], 'left': []},
            1: {'top': top, 'right': [], 'bottom': bottom, 'left': []},
            2: {'top': [], 'right': right, 'bottom': [], 'left': left},
            3: {'top': [], 'right': right, 'bottom': bottom, 'left':[]},
            4: {'top': [], 'right': [], 'bottom': bottom, 'left': left},
            5: {'top': top, 'right': right, 'bottom': [], 'left': []},
            6: {'top': top, 'right': [], 'bottom': [], 'left': left},
            7: {'top': top, 'right': [], 'bottom': bottom, 'left': left},
            8: {'top': top, 'right': right, 'bottom': bottom, 'left': []},
            9: {'top': top, 'right': right, 'bottom': [], 'left': left},
            10: {'top': [], 'right': right, 'bottom': bottom, 'left': left},
            11: {'top': top, 'right': right, 'bottom': bottom, 'left': left},
            }

    def distance(self, point_two, point_one=''):
        if not point_one:
            point_one = [self.me.x, self.me.y]
        return sqrt((point_one[0] - point_two[0])**2 + (point_one[1] - point_two[1])**2)

    def manhattan_distance(self, point_two, point_one=''):
        if not point_one:
            point_one = self.current_tilepoint
        return abs(point_one[0] - point_two[0]) + abs(point_one[1] - point_two[1])

    def successors(self, point):
        succ = []
        for i, (x, y) in enumerate([(0, -1), (1, 0), (0, 1), (-1, 0)]):
            if (0 <= point[0] + x <= len(self.world.tiles_x_y)-1) and (0 <= point[1] + y <= len(self.world.tiles_x_y[0])-1):
                if i == 0:
                    side = 'top'
                elif i == 1:
                    side = 'right'
                elif i == 2:
                    side = 'bottom'
                else:
                    side = 'left'
                if self.world.tiles_x_y[point[0] + x][point[1] + y] in self.accessible_tiles[self.world.tiles_x_y[point[0]][point[1]]][side]:
                    succ.append([point[0] + x, point[1] + y])
        if len(succ) > 1:
            return succ
        else:
            return ['unknown']

    def search_path(self, start_point, finish_point, last_point=[]):  # A*
        #path_found = []
        p_queue = PriorityQueue()
        p_queue.push([[start_point], 0], 0)
        #if not last_point:
        #    last_point = self.last_tilepoint
        #print('sfl', start_point, finish_point, last_point)
        while 1:
            if not p_queue.isEmpty():
                current_state = p_queue.pop()
            else:
                return self.search_path(start_point, finish_point, ['x', 'y'])
            if current_state[0][-1] == finish_point:
                #path_found.append(current_state[0])
                return current_state[0]
            else:
                #print('for:', current_state[0][-1], 'its', self.world.tiles_x_y[current_state[0][-1][0]][current_state[0][-1][1]])
                for state in self.successors(current_state[0][-1]):
                    #print(state)
                    if state == 'unknown':  # warning it will return path in first time occured unknown tile
                        return current_state[0]
                    elif state not in current_state[0] and state != last_point:
                        next_state = current_state[0][:]
                        next_state.append(state)
                        h = self.manhattan_distance(finish_point)
                        p_queue.push([next_state, current_state[1] + 1], current_state[1] + 1 + h)
            #else:
            #    return path_found

    def turn_type(self):
        square = [[0, 0, 0],
                  [0, 1, 0],
                  [0, 0, 0]]
        if self.current_tilepoint[0] - self.second_tilepoint[0] == 1:
            x = 0
        elif self.current_tilepoint[0] - self.second_tilepoint[0] == 0:
            x = 1
        else:
            x = 2
        if self.current_tilepoint[1] - self.second_tilepoint[1] == 1:
            y = 0
        elif self.current_tilepoint[1] - self.second_tilepoint[1] == 0:
            y = 1
        else:
            y = 2
        square[y][x] = 1  #self.before_turn_waypoint
        x = 4
        y = 4
        if self.current_tilepoint[0] - self.third_tilepoint[0] == 1:
            x = 0
        elif self.current_tilepoint[0] - self.third_tilepoint[0] == 0:
            x = 1
        elif self.current_tilepoint[0] - self.third_tilepoint[0] == -1:
            x = 2
        if self.current_tilepoint[1] - self.third_tilepoint[1] == 1:
            y = 0
        elif self.current_tilepoint[1] - self.third_tilepoint[1] == 0:
            y = 1
        elif self.current_tilepoint[1] - self.third_tilepoint[1] == -1:
            y = 2
        try:
            square[y][x] = 1  #self.turn_waypoint
        except IndexError:
            pass

        types = {'foward': [[[0, 1, 0],
                             [0, 1, 0],
                             [0, 0, 0]],
                            [[0, 0, 0],
                             [0, 1, 1],
                             [0, 0, 0]],
                            [[0, 0, 0],
                             [0, 1, 0],
                             [0, 1, 0]],
                            [[0, 0, 0],
                             [1, 1, 0],
                             [0, 0, 0]]],
                 'bottom_to_left': [[1, 1, 0],
                                    [0, 1, 0],
                                    [0, 0, 0]],
                 'bottom_to_right': [[0, 1, 1],
                                     [0, 1, 0],
                                     [0, 0, 0]],
                 'top_to_right': [[0, 0, 0],
                                  [0, 1, 0],
                                  [0, 1, 1]],
                 'top_to_left': [[0, 0, 0],
                                 [0, 1, 0],
                                 [1, 1, 0]],
                 'right_to_top': [[1, 0, 0],
                                  [1, 1, 0],
                                  [0, 0, 0]],
                 'right_to_bottom': [[0, 0, 0],
                                     [1, 1, 0],
                                     [1, 0, 0]],
                 'left_to_bottom': [[0, 0, 0],
                                    [0, 1, 1],
                                    [0, 0, 1]],
                 'left_to_top': [[0, 0, 1],
                                 [0, 1, 1],
                                 [0, 0, 0]],
                 }
        for name, scheme in types.items():
            if square in scheme or square == scheme:
                return name
        for row in square:
            print(row)
        return "Unknown"

    def rudder_position(self):
        angle_coefficient = 15
        entry_coef = 1.6
        current_type = self.turn_type()

        #print(current_type)
        if self.near_bonus and current_type == 'foward':
            entry_point_x = self.near_bonus.x
            entry_point_y = self.near_bonus.y
            angle_coefficient = 30
        elif current_type in ('bottom_to_left', 'left_to_bottom'):
            entry_point_x = self.second_tilepoint[0]*self.game.track_tile_size + self.game.track_tile_margin*entry_coef
            entry_point_y = (self.second_tilepoint[1]+1)*self.game.track_tile_size - self.game.track_tile_margin*entry_coef
        elif current_type in ('bottom_to_right', 'right_to_bottom'):
            entry_point_x = (self.second_tilepoint[0]+1)*self.game.track_tile_size - self.game.track_tile_margin*entry_coef
            entry_point_y = (self.second_tilepoint[1]+1)*self.game.track_tile_size - self.game.track_tile_margin*entry_coef
        elif current_type in ('top_to_right', 'right_to_top'):
            entry_point_x = (self.second_tilepoint[0]+1)*self.game.track_tile_size - self.game.track_tile_margin*entry_coef
            entry_point_y = self.second_tilepoint[1]*self.game.track_tile_size + self.game.track_tile_margin*entry_coef
        elif current_type in ('top_to_left', 'left_to_top'):
            entry_point_x = self.second_tilepoint[0]*self.game.track_tile_size + self.game.track_tile_margin*entry_coef
            entry_point_y = self.second_tilepoint[1]*self.game.track_tile_size + self.game.track_tile_margin*entry_coef
        else:
            entry_point_x = (self.second_tilepoint[0]+0.5)*self.game.track_tile_size
            entry_point_y = (self.second_tilepoint[1]+0.5)*self.game.track_tile_size
            angle_coefficient = 5
        return angle_coefficient*self.me.get_angle_to(entry_point_x, entry_point_y)/pi

    def print_bonus(self):
        if self.near_bonus.type == 0:
            print("REPAIR_KIT")
        elif self.near_bonus.type == 1:
            print("AMMO_CRATE")
        elif self.near_bonus.type == 2:
            print("NITRO_BOOST")
        elif self.near_bonus.type == 3:
            print("OIL_CANISTER")
        elif self.near_bonus.type == 4:
            print("PURE_SCORE")

    def move(self, me, world, game, move):
        self.world = world
        self.me = me
        self.game = game
        self._move = move
        speedModule = hypot(me.speed_x, me.speed_y)
        self.speed_track.push(speedModule)

        # bonus
        for bonus in world.bonuses:
            distance = self.distance([bonus.x, bonus.y])
            angle = abs(180*me.get_angle_to_unit(bonus)/pi)
            tile = [int(bonus.x//game.track_tile_size), int(bonus.y//game.track_tile_size)]

            if (angle < 13 and 600 < distance < 1300) and tile in [self.current_tilepoint, self.second_tilepoint, self.third_tilepoint]:
                self.last_bonus = self.near_bonus
                self.near_bonus = bonus
                break
            else:
                self.near_bonus = None

        # path
        if [me.next_waypoint_x, me.next_waypoint_y] != self.last_waypoint:
            self.next_waypoint_number += 1
            self.last_waypoint = [me.next_waypoint_x, me.next_waypoint_y]

        if self.last_tilepoint == ['x', 'y']:
            self.current_tilepoint = [int(me.x//game.track_tile_size), int(me.y//game.track_tile_size)]
            if world.starting_direction == 0:
                self.last_tilepoint = [self.current_tilepoint[0]+1, self.current_tilepoint[1]]
            elif world.starting_direction == 1:
                self.last_tilepoint = [self.current_tilepoint[0]-1, self.current_tilepoint[1]]
            elif world.starting_direction == 2:
                self.last_tilepoint = [self.current_tilepoint[0], self.current_tilepoint[1]+1]
            elif world.starting_direction == 3:
                self.last_tilepoint = [self.current_tilepoint[0], self.current_tilepoint[1]-1]
            self.current_tilepoint = self.last_tilepoint

        if self.current_tilepoint != [int(me.x//game.track_tile_size), int(me.y//game.track_tile_size)]:
            self.last_tilepoint = self.current_tilepoint
            self.current_tilepoint = [int(me.x//game.track_tile_size), int(me.y//game.track_tile_size)]
            path = self.search_path(self.current_tilepoint, [me.next_waypoint_x, me.next_waypoint_y])
            i = 0
            while len(path) < 6 and not self.invisible:
                through_waypoint = world.waypoints[(self.next_waypoint_number + i) % len(world.waypoints)]
                i+=1
                through_one_waypoint = world.waypoints[(self.next_waypoint_number + i) % len(world.waypoints)]
                path += self.search_path(through_waypoint, through_one_waypoint)[1:]
            self.path = path
            self.second_tilepoint = path[1]
            self.third_tilepoint = path[2]
            if len(path) > 3:
                self.fourth_tilepoint = path[3]
            else:
                self.fourth_tilepoint = None
            if len(path) > 4:
                self.fifth_tilepoint = path[4]
            else:
                self.fifth_tilepoint = None

        # rudder
        if self.current_turn != self.turn_type():
            self.last_turn = self.current_turn
            self.current_turn = self.turn_type()
        move.wheel_turn = self.rudder_position()
        turn = Turn(self.current_tilepoint, self.second_tilepoint, self.third_tilepoint, self.fourth_tilepoint)
        print(turn.type)
        print(self.path)
        #  brake
        next_x = (self.second_tilepoint[0] + 0.5) * game.track_tile_size
        next_y = (self.second_tilepoint[1] + 0.5) * game.track_tile_size
        if (speedModule**2) * abs(me.get_angle_to(next_x, next_y)) > pi*5**2 and speedModule > 16:  # 4-6 optimal
            move.brake = True
        elif world.tiles_x_y[self.third_tilepoint[0]][self.third_tilepoint[1]] in [3, 4, 5, 6] and speedModule > 25:
            move.brake = True

        # nitro
        part_type = []
        if len(self.path) > 3:
            for p in self.path[:4]:
                part_type.append(world.tiles_x_y[p[0]][p[1]])
        if game.initial_freeze_duration_ticks < world.tick and len(set(part_type)) == 1:
            move.use_nitro = True
        elif game.initial_freeze_duration_ticks < world.tick < game.initial_freeze_duration_ticks + 2:
            move.use_nitro = True

        # oil and shot
        for car in world.cars:
            if car.player_id != world.get_my_player().id:
                if -0.06 < me.get_angle_to(car.x, car.y) < 0.06 and self.distance([car.x, car.y]) < 1800:
                    move.throw_projectile = True

                if (-pi <= me.get_angle_to(car.x, car.y) < -3 or 3 < me.get_angle_to(car.x, car.y) <= pi)\
                        and 200 < self.distance([car.x, car.y]) < 1200:
                    self.use_oil_in_next_turn = True
            else:
                if car.oil_canister_count > 1:
                    self.use_oil_in_next_turn = True

        if (self.use_oil_in_next_turn and self.last_turn != 'foward') \
                or self.distance([(world.waypoints[-1][0]+0.5)*game.track_tile_size, (world.waypoints[-1][1]+0.5)*game.track_tile_size]) < 300:
            move.spill_oil = True
            self.use_oil_in_next_turn = False

        # U-turn
        if self.speed_track.average < 2 and world.tick > game.initial_freeze_duration_ticks+50:
            if self.u_turn_counter > 100:
                entry_point_x = (self.second_tilepoint[0]+0.5)*self.game.track_tile_size
                entry_point_y = (self.second_tilepoint[1]+0.5)*self.game.track_tile_size
                move.wheel_turn = -30*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
                move.engine_power = -1
                self.u_turn_counter -= 1
            elif 0 < self.u_turn_counter <= 100:
                entry_point_x = (self.second_tilepoint[0]+0.5)*self.game.track_tile_size
                entry_point_y = (self.second_tilepoint[1]+0.5)*self.game.track_tile_size
                move.wheel_turn = 30*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
                move.engine_power = 1
                self.u_turn_counter -= 1
                self.stop = True
            elif self.u_turn_counter == 0:
                self.u_turn_counter = 200
            else:
                move.engine_power = 1
        else:
            move.engine_power = 1
            if 0 < self.u_turn_counter < 200:
                entry_point_x = (self.second_tilepoint[0]+0.5)*self.game.track_tile_size
                entry_point_y = (self.second_tilepoint[1]+0.5)*self.game.track_tile_size
                if self.stop:
                    move.wheel_turn = 30*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
                else:
                    move.wheel_turn = -30*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
                self.u_turn_counter -= 1
            else:
                self.u_turn_counter = 200
                self.stop = False

        '''
        for p in path:
            print(self.current_tilepoint, [me.next_waypoint_x, me.next_waypoint_y])
            for y in range(len(self.world.tiles_x_y[0])):
                row = ''
                for x in range(len(self.world.tiles_x_y)):
                    if [x,y] in p:
                        row += "* "
                    else:
                        row += str(world.tiles_x_y[x][y]) + " "
                print(row)
            print(p)
            print('----------------------')
            '''
