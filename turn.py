class Turn:
    def __init__(self, fi, s, t, fo):
        self.first = fi
        self.second = s
        self.third = t
        self.fourth = fo
        self.square = [[0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 1, 0, 0],
                       [0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0]]

        if self.first[0] - self.second[0] == 1: x = 1
        elif self.first[0] - self.second[0] == 0: x = 2
        else: x = 3
        if self.first[1] - self.second[1] == 1: y = 1
        elif self.first[1] - self.second[1] == 0: y = 2
        else: y = 3
        self.square[y][x] = 1  # second

        if self.first[0] - self.third[0] == 2: x = 0
        elif self.first[0] - self.third[0] == 1: x = 1
        elif self.first[0] - self.third[0] == 0: x = 2
        elif self.first[0] - self.third[0] == -1: x = 3
        else: x = 4
        if self.first[1] - self.third[1] == 2: y = 0
        elif self.first[1] - self.third[1] == 1: y = 1
        elif self.first[1] - self.third[1] == 0: y = 2
        elif self.first[1] - self.third[1] == -1: y = 3
        else: y = 4
        self.square[y][x] = 1  # third

        if self.first[0] - self.fourth[0] == 2: x = 0
        elif self.first[0] - self.fourth[0] == 1: x = 1
        elif self.first[0] - self.fourth[0] == 0: x = 2
        elif self.first[0] - self.fourth[0] == -1: x = 3
        elif self.first[0] - self.fourth[0] == -2: x = 4
        else: x = 5
        if self.first[1] - self.fourth[1] == 2: y = 0
        elif self.first[1] - self.fourth[1] == 1: y = 1
        elif self.first[1] - self.fourth[1] == 0: y = 2
        elif self.first[1] - self.fourth[1] == -1: y = 3
        elif self.first[1] - self.fourth[1] == -2: y = 4
        else: y = 5

        try:
            self.square[y][x] = 1  # fourth
        except IndexError:
            pass

        self.type = self.relation()

    def rotate90(self, square):
        size = len(square)-1
        new_square = []
        for y in range(size+1):
            row = []
            for x in range(size, -1, -1):
                row.append(square[x][y])
            new_square.append(row)
        return new_square

    def rotate270(self, square):
        size = len(square)-1
        new_square = []
        for y in range(size, -1, -1):
            row = []
            for x in range(size+1):
                row.append(square[x][y])
            new_square.append(row)
        return new_square

    def relation(self):
        ''' T = top
            R = right
            B = bottom
            L = left '''
        _ = 0
        TTT = [[_, _, 1, _, _],
               [_, _, 1, _, _],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TLL = [[_, _, _, _, _],
               [1, 1, 1, _, _],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TLB = [[_, _, _, _, _],
               [_, 1, 1, _, _],
               [_, 1, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TLT = [[_, 1, _, _, _],
               [_, 1, 1, _, _],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TTL = [[_, 1, 1, _, _],
               [_, _, 1, _, _],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TTR = [[_, _, 1, 1, _],
               [_, _, 1, _, _],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TRT = [[_, _, _, 1, _],
               [_, _, 1, 1, _],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TRB = [[_, _, _, _, _],
               [_, _, 1, 1, _],
               [_, _, 1, 1, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        TRR = [[_, _, _, _, _],
               [_, _, 1, 1, 1],
               [_, _, 1, _, _],
               [_, _, _, _, _],
               [_, _, _, _, _]]
        types = {'TTT': TTT,
                 'TLL': TLL,
                 'TLB': TLB,
                 'TLT': TLT,
                 'TTL': TTL,
                 'TTR': TTR,
                 'TRT': TRT,
                 'TRB': TRB,
                 'TRR': TRR,
                 'RRR': self.rotate90(TTT),
                 'RTT': self.rotate90(TLL),
                 'RTL': self.rotate90(TLB),
                 'RTR': self.rotate90(TLT),
                 'RRT': self.rotate90(TTL),
                 'RRB': self.rotate90(TTR),
                 'RBR': self.rotate90(TRT),
                 'RBL': self.rotate90(TRB),
                 'RBB': self.rotate90(TRR),
                 'BBB': self.rotate90(self.rotate90(TTT)),
                 'BRR': self.rotate90(self.rotate90(TLL)),
                 'BRT': self.rotate90(self.rotate90(TLB)),
                 'BRB': self.rotate90(self.rotate90(TLT)),
                 'BBR': self.rotate90(self.rotate90(TTL)),
                 'BBL': self.rotate90(self.rotate90(TTR)),
                 'BLB': self.rotate90(self.rotate90(TRT)),
                 'BLT': self.rotate90(self.rotate90(TRB)),
                 'BLL': self.rotate90(self.rotate90(TRR)),
                 'LLL': self.rotate270(TTT),
                 'LBB': self.rotate270(TLL),
                 'LBR': self.rotate270(TLB),
                 'LBL': self.rotate270(TLT),
                 'LLB': self.rotate270(TTL),
                 'LLT': self.rotate270(TTR),
                 'LTL': self.rotate270(TRT),
                 'LTR': self.rotate270(TRB),
                 'LTT': self.rotate270(TRR),
                 }
        for name, scheme in types.items():
            if self.square == scheme:
                return name
        return "Unknown"

if __name__ == '__main__':
    _ = 0
    sq = [[_, _, _, _, _],
          [_, _, 1, 1, 1],
          [_, _, 1, _, _],
          [_, _, _, _, _],
          [_, _, _, _, _]]
    tu = Turn([0,0], [0,1], [1,1], [1,2])
    #for row in tu.rotate270(sq):
    #    print row
    tu.type()


'''
from model.TileType import TileType as T

def turn(self):
    if self.world.tiles_x_y[self.turn_waypoint[0]][self.turn_waypoint[1]] == T.LEFT_TOP_CORNER:
        if self.turn_waypoint[0] == self.current_tilepoint[0]:  # car arrive from bottom
            entry_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size + self.game.car_width
            entry_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size
        else:  # car arrive from right
            entry_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size
            entry_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size + self.game.car_width
        self.move.wheel_turn = 10*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
    elif self.world.tiles_x_y[self.before_turn_waypoint[0]][self.before_turn_waypoint[1]] == T.LEFT_TOP_CORNER:
        angle_point_x = (self.before_turn_waypoint[0]+1)*self.game.track_tile_size
        angle_point_y = (self.before_turn_waypoint[1]+1)*self.game.track_tile_size
        self.move.wheel_turn = 2*self.me.get_angle_to(angle_point_x, angle_point_y)/pi

    elif self.world.tiles_x_y[self.turn_waypoint[0]][self.turn_waypoint[1]] == T.RIGHT_TOP_CORNER:
        if self.turn_waypoint[0] == self.current_tilepoint[0]:  # car arrive from bottom
            entry_point_x = (self.before_turn_waypoint[0] + 1)*self.game.track_tile_size - self.game.car_width
            entry_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size
        else:  # car arrive from left
            entry_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size
            entry_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size + self.game.car_width
        self.move.wheel_turn = 10*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
    elif self.world.tiles_x_y[self.before_turn_waypoint[0]][self.before_turn_waypoint[1]] == T.RIGHT_TOP_CORNER:
        angle_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size
        angle_point_y = (self.before_turn_waypoint[1]+1)*self.game.track_tile_size
        self.move.wheel_turn = 2*self.me.get_angle_to(angle_point_x, angle_point_y)/pi

    elif self.world.tiles_x_y[self.turn_waypoint[0]][self.turn_waypoint[1]] == T.RIGHT_BOTTOM_CORNER:
        if self.turn_waypoint[0] == self.current_tilepoint[0]:  # car arrive from top
            entry_point_x = (self.before_turn_waypoint[0] + 1)*self.game.track_tile_size - self.game.car_width
            entry_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size
        else: # car arrive from left
            entry_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size
            entry_point_y = (self.before_turn_waypoint[1] + 1)*self.game.track_tile_size - self.game.car_width
        self.move.wheel_turn = 10*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
    elif self.world.tiles_x_y[self.before_turn_waypoint[0]][self.before_turn_waypoint[1]] == T.RIGHT_BOTTOM_CORNER:
        angle_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size
        angle_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size
        self.move.wheel_turn = 2*self.me.get_angle_to(angle_point_x, angle_point_y)/pi

    elif self.world.tiles_x_y[self.turn_waypoint[0]][self.turn_waypoint[1]] == T.LEFT_BOTTOM_CORNER:
        if self.turn_waypoint[0] == self.current_tilepoint[0]:  # car arrive from top
            entry_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size + self.game.car_width
            entry_point_y = (self.before_turn_waypoint[1]+1)*self.game.track_tile_size
        else:  # car arrive from right
            entry_point_x = self.before_turn_waypoint[0]*self.game.track_tile_size
            entry_point_y = (self.before_turn_waypoint[1] + 1)*self.game.track_tile_size - self.game.car_width
        self.move.wheel_turn = 10*self.me.get_angle_to(entry_point_x, entry_point_y)/pi
    elif self.world.tiles_x_y[self.before_turn_waypoint[0]][self.before_turn_waypoint[1]] == T.LEFT_BOTTOM_CORNER:
        angle_point_x = (self.before_turn_waypoint[0]+1)*self.game.track_tile_size
        angle_point_y = self.before_turn_waypoint[1]*self.game.track_tile_size
        self.move.wheel_turn = 2*self.me.get_angle_to(angle_point_x, angle_point_y)/pi

    else:
        exit_point_x = (self.before_turn_waypoint[0]+0.5)*self.game.track_tile_size
        exit_point_y = (self.before_turn_waypoint[1]+0.5)*self.game.track_tile_size
        self.move.wheel_turn = 10*self.me.get_angle_to(exit_point_x, exit_point_y)
'''