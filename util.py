import heapq


class PriorityQueue:
    def __init__(self):
        self.heap = []
        self.count = 0

    def push(self, item, priority):
        entry = (priority, self.count, item)
        heapq.heappush(self.heap, entry)
        self.count += 1

    def pop(self):
        (_, _, item) = heapq.heappop(self.heap)
        return item

    def isEmpty(self):
        return len(self.heap) == 0

    def echo(self):
        return self.heap

class FixedQueue:
    def __init__(self, lenght=10):
        self.queue = []
        self.length = lenght
        self.average = 0

    def __getitem__(self, key):
        return self.queue[key]

    def __str__(self):
        return str(self.queue)

    def push(self, item):
            self.queue.append(item)
            if len(self.queue) > self.length:
                self.pop()
            self._average()

    def pop(self):
        first = self.queue.pop(0)
        self._average()
        return first

    def _average(self):
        self.average = sum(self.queue)/len(self.queue)



"""
def search_path(self, start_point, finish_point, last_point=[]):  # in depth
    stack = [[start_point]]
    path_found = []
    if not last_point:
        last_point = self.last_tilepoint
    #print(start_point)
    #print(finish_point)
    while 1:
        #print(stack)
        try:
            current_state = stack.pop()
            if current_state[-1] == finish_point:
                path_found.append(current_state)
            else:
                for state in self.successors(current_state[-1]):
                    #print('*',current_state[-1], state)
                    if state not in current_state and state != last_point:
                        next_state = current_state[:]
                        next_state.append(state)
                        stack.append(next_state)
        except IndexError:
            return path_found
"""